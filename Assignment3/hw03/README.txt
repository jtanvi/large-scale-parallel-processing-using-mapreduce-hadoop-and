INSTRUCTIONS:

There are 2 makefiles present in this folder. One is for standalone and pseudo-cluster local run, and another one (makefile_aws) is for running on emr. Please change the paths in the makefiles for your environment.

The Program is divided as follows:
1. Main (Runs all the MapReduce Jobs)
2. DataParser (For Data Pre-Processing)
3. PageRank (For computing page ranks)
4. TopK (For computing top 100 highest ranked pages)
5. GlobalCounters (Updating and tracking counters through all the job runs)
6. Node (Object to store page attributes)