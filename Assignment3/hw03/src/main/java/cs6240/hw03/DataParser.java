package cs6240.hw03;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;


/**
 * Class DataParser
 * This parser was provided and is being used to extract page name and its outlinks
 * from a given html page. The code includes logic for incrementing a global counter
 * (number of pages) and snippet shared in the discussion by Justin Koser to handle UTF 
 * encodings in the record
 */
public class DataParser {

	public static class ParsingMapper extends Mapper<Object, Text, Text, Node>{
		
		int count = 0;
		private static Pattern namePattern;
		private static Pattern linkPattern;
		static {
			// Keep only html pages not containing tilde (~).
			namePattern = Pattern.compile("^([^~|\\%]+)$");
			// Keep only html filenames ending relative paths and not containing tilde (~).
			linkPattern = Pattern.compile("^\\..*/([^~|\\%]+)\\.html$");
	
		}
	
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			Text nodeID = new Text();
			
			try {
				// Configure parser.
				SAXParserFactory spf = SAXParserFactory.newInstance();
				spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
				SAXParser saxParser = spf.newSAXParser();
				XMLReader xmlReader = saxParser.getXMLReader();
				// Parser fills this list with linked page names.
				List<String> linkPageNames = new ArrayList<>();
				xmlReader.setContentHandler(new WikiParser(linkPageNames));
				
			    String pageName = new String(Arrays.copyOf(value.getBytes(), value.find(":")), "latin1");
				String record = value.toString();
				int delimLoc = record.indexOf(':');
				String html = record.substring(delimLoc + 1).replaceAll("&(?!amp;)", "&amp;");
				Matcher matcher = namePattern.matcher(pageName);
				if (!matcher.find()) {
					return;
				}
	
				// Parse page and fill list of linked pages.
				linkPageNames.clear();
				try {
					xmlReader.parse(new InputSource(new StringReader(html)));
				} catch (Exception e) {
					// Discard ill-formatted pages.
					return;
				}
	
				// Page found, increment the counter
				context.getCounter(GlobalCounters.NUM_OF_PAGES).increment(1);
				
				// Extract unique neighbor links
				List<String> neighborPagesNames = linkPageNames.stream().distinct().collect(Collectors.toList());
				ArrayList<String> neighborPages = new ArrayList<String>(neighborPagesNames);
				
				// Create Node object to include the adjacency list 'neighborPages'
				Node node = new Node();
				node.setNodeID(pageName);
				node.setPageRank(0.0);
				node.setIsPGContribution(false);
				node.setAdjList(neighborPages);
				nodeID.set(pageName);
				
				// If the given page does not have any outlinks, increment the counter
				if (neighborPages.isEmpty()) {
					context.getCounter(GlobalCounters.DANGLERS_BEFORE).increment(1);
				}
				
				// Emit the pageName and its Node object
				context.write(nodeID, node);
				
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	
		/** Parses a Wikipage, finding links inside bodyContent div element. */
		private static class WikiParser extends DefaultHandler {
			/** List of linked pages; filled by parser. */
			private List<String> linkPageNames;
			/** Nesting depth inside bodyContent div element. */
			private int count = 0;
	
			public WikiParser(List<String> linkPageNames) {
				super();
				this.linkPageNames = linkPageNames;
			}
	
			@Override
			public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
				super.startElement(uri, localName, qName, attributes);
				if ("div".equalsIgnoreCase(qName) && "bodyContent".equalsIgnoreCase(attributes.getValue("id")) && count == 0) {
					// Beginning of bodyContent div element.
					count = 1;
				} else if (count > 0 && "a".equalsIgnoreCase(qName)) {
					// Anchor tag inside bodyContent div element.
					count++;
					String link = attributes.getValue("href");
					if (link == null) {
						return;
					}
					try {
						// Decode escaped characters in URL.
						link = URLDecoder.decode(link, "UTF-8");
					} catch (Exception e) {
						// Wiki-weirdness; use link as is.
					}
					// Keep only html filenames ending relative paths and not containing tilde (~).
					Matcher matcher = linkPattern.matcher(link);
					if (matcher.find()) {
						linkPageNames.add(matcher.group(1));
					}
				} else if (count > 0) {
					// Other element inside bodyContent div.
					count++;
				}
			}
	
			@Override
			public void endElement(String uri, String localName, String qName) throws SAXException {
				super.endElement(uri, localName, qName);
				if (count > 0) {
					// End of element inside bodyContent div.
					count--;
				}
			}
		}
	}
}
	
	
	


