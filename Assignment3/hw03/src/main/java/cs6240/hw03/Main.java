package cs6240.hw03;

import java.io.IOException;



import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;


import cs6240.hw03.DataParser.ParsingMapper;
import cs6240.hw03.PageRank.PageRankMapper;
import cs6240.hw03.PageRank.PageRankReducer;
import cs6240.hw03.TopK.TopKMapper;
import cs6240.hw03.TopK.TopKReducer;


/*
 * Class Main
 * Data parsing to extract nodes (pages) and their corresponding adjacency
 * lists (outlinks) and compute pageranks of all pages. 
 */
public class Main {
	
	public static void main(String[] args) throws Exception {
		
		System.out.println("Starting Data Parser...");
	    Configuration conf = new Configuration();
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length < 1) {
	      System.err.println("Provide args for input files and output path");
	      System.exit(2);
	    }
	    
	    // Setting up the 1st MapReduce Pre-Processing Job
	    Job currentJob = preProcessingJob(conf, otherArgs); 
	    System.out.println("Number of pages: " +  currentJob.getCounters().findCounter(GlobalCounters.NUM_OF_PAGES).getValue()); //conf.get("NUM_OF_PAGES"));
		System.out.println("Number of dangling nodes: " + currentJob.getCounters().findCounter(GlobalCounters.DANGLERS_BEFORE).getValue());
	    
		// Paths for the next set of map-reduce (PageRank) Jobs
		String inputPath = otherArgs[otherArgs.length - 1] + "/dataRecords";
		String outputPathPrefix = otherArgs[otherArgs.length - 1];
		String outputPath;
		
		
		// Global Counter setup 
		Counters jobCounters = currentJob.getCounters();
		Counter pageCounter = jobCounters.findCounter(GlobalCounters.NUM_OF_PAGES);		
		conf.set(pageCounter.getDisplayName().toString(), Double.toString(pageCounter.getValue()));

		
		// Run 10 iterations of PageRank Job
	    for (int i = 1; i < 11; i++) {
	    	
	    		outputPath = outputPathPrefix + "/" + i;
	    	
	    		// Counter for keeping track of iterations
	    		Counter itCounter = jobCounters.findCounter(GlobalCounters.ITERATION_NUM);
	    		itCounter.increment(1);
	    		conf.set(itCounter.getDisplayName().toString(), Double.toString(itCounter.getValue()));
	    		
	    		// Update the delta contribution on every iteration
	    		Counter deltaCounter = currentJob.getCounters().findCounter(GlobalCounters.DANGLINGNODES_CONTRIBUTION);
	    		conf.set(deltaCounter.getDisplayName().toString(), Double.toString(deltaCounter.getValue()));
	    		//System.out.println("Iteration si>> " + itCounter.getValue() + " and " + deltaCounter.getValue());
	    		
	    		// Run PageRank
	    		Job job = runPageRankJob(conf, inputPath, outputPath);
	    		currentJob = job;
	    		inputPath = outputPath; 
	    		
	    		// Counter to check convergence of total pageranks
	    		Counter totalPR = currentJob.getCounters().findCounter(GlobalCounters.PAGERANK_SO_FAR);
	        //System.out.println("Total page rank : " + (totalPR.getValue()/Math.pow(10, 9)));	
	    }
	    
		
		// Find top-100 Pages based on their pageranks
	    outputPath = outputPathPrefix + "/topK";
	    Job topKJob = runTopK(conf, inputPath, outputPath);
	}
	
	
	/**
	 * runTopK: MapReduce job to output top 100 pages with high pageranks (in descending order)
	 * @param conf
	 * @param inputPath
	 * @param outputPath
	 * @return Job
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 */
	public static Job runTopK(Configuration conf, String inputPath, String outputPath) throws IOException, ClassNotFoundException, InterruptedException {
		System.out.println("in top k method");
		Job job = Job.getInstance(conf, "Starting Top K");
		job.setJarByClass(Main.class);
	    
	    job.setMapperClass(TopKMapper.class);
	    job.setReducerClass(TopKReducer.class);
	    
	    job.setMapOutputKeyClass(NullWritable.class);
	    job.setMapOutputValueClass(Text.class);
	    job.setOutputKeyClass(NullWritable.class);
	    job.setOutputValueClass(Text.class);
	    job.setInputFormatClass(SequenceFileInputFormat.class);
	    
	    FileInputFormat.addInputPath(job, new Path(inputPath));
        FileOutputFormat.setOutputPath(job, new Path(outputPath));
        
        job.waitForCompletion(true);
	    return job;
	}
	
	/**
	 * runPageRankJob: computes pageranks of all pages; one job for one iteration
	 * @param conf
	 * @param inputPath
	 * @param outputPath
	 * @return Job
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 */
	public static Job runPageRankJob(Configuration conf, String inputPath, String outputPath) throws IOException, ClassNotFoundException, InterruptedException {
		System.out.println("in pagerank method");
		Job job = Job.getInstance(conf, "Starting Page Rank");
		job.setJarByClass(Main.class);
	    
	    job.setMapperClass(PageRankMapper.class);
	    job.setReducerClass(PageRankReducer.class);
	    
	    job.setMapOutputKeyClass(Text.class);
	    job.setMapOutputValueClass(Node.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Node.class);
	    job.setInputFormatClass(SequenceFileInputFormat.class);
	    job.setOutputFormatClass(SequenceFileOutputFormat.class);
	    
	    FileInputFormat.addInputPath(job, new Path(inputPath));
        FileOutputFormat.setOutputPath(job, new Path(outputPath));
        
        job.waitForCompletion(true);
	    return job;
	}
	
	/**
	 * preProcesingJob: Parses the given bz2 files and extract nodes and their adjacency lists
	 * @param conf
	 * @param otherArgs
	 * @return Job
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 */
	public static Job preProcessingJob(Configuration conf, String[] otherArgs) throws IOException, ClassNotFoundException, InterruptedException {
		Job job = Job.getInstance(conf, "Data Parser");
	    job.setJarByClass(Main.class);
	    
	    job.setMapperClass(ParsingMapper.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Node.class);
	    job.setOutputFormatClass(SequenceFileOutputFormat.class);
	    
	    for (int i = 0; i < otherArgs.length - 1; ++i) {
	      FileInputFormat.addInputPath(job, new Path(otherArgs[i]));
	    }
	    FileOutputFormat.setOutputPath(job,
	    new Path(otherArgs[otherArgs.length - 1] + "/dataRecords"));
	    
	    job.waitForCompletion(true);	    
	    return job;
	}
	
}
