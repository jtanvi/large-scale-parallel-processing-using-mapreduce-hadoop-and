package cs6240.hw03;


public enum GlobalCounters {
	
	ITERATION_NUM,	// Keeps track of iterations
	NUM_OF_PAGES,	// Counts the total number of pages in the preprocessing job
	DANGLINGNODES_CONTRIBUTION, // Counter to add up delta in every iteration
	DANGLERS_BEFORE,	// Counts the total number of dangling nodes
	PAGERANK_SO_FAR; // Sums up the pageranks for all pages in one iteration
}
