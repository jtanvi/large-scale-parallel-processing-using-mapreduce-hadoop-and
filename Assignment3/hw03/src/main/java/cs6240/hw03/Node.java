package cs6240.hw03;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableUtils;

/**
 * Class Node
 * Custom object that stores attributes like page-rank, adjacency list
 * page-name and flag to check if the page-rank is parent page's contribution
 *
 */
public class Node implements Writable {
	
	private String nodeID;
	private double pageRank;
	private ArrayList<String> adjList;
	private Boolean isPGContribution;
	
	public Boolean getIsPGContribution() {
		return this.isPGContribution;
	}
	
	public void setIsPGContribution(boolean bool) {
		this.isPGContribution = bool;
	}

	public String getNodeID() {
		return nodeID;
	}

	public void setNodeID(String nodeID) {
		this.nodeID = nodeID;
	}

	public double getPageRank() {
		return pageRank;
	}

	public void setPageRank(double pageRank) {
		this.pageRank = pageRank;
	}

	public ArrayList<String> getAdjList() {
		return adjList;
	}

	public void setAdjList(ArrayList<String> adjList) {
		this.adjList = adjList;
	}
	
	public void write(DataOutput out)throws IOException{
		WritableUtils.writeString(out, nodeID);
		out.writeDouble(pageRank);
		out.writeBoolean(isPGContribution);
		out.writeInt(adjList.size());
		for (int i = 0; i < adjList.size(); i++) {
			out.writeUTF(adjList.get(i));
		}
		
	}
	
	public void readFields(DataInput in) throws IOException{
		nodeID = WritableUtils.readString(in);
		pageRank = in.readDouble();
		isPGContribution = in.readBoolean();
		int listSize = in.readInt();
		adjList = new ArrayList<String>();
        for (int i = 0; i < listSize; i++) {
            adjList.add(in.readUTF());
        }
	}

}
