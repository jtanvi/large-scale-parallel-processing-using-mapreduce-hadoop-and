package cs6240.hw03;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Counters;
//import org.apache.hadoop.mapred.Counters.Counter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;


public class TopK {

	/*
	 * Class TopKMapper
	 * Takes the nid and pagerank of the given pages and computes local top 100
	 * pages with high pageranks
	 */
	public static class TopKMapper extends Mapper<Text, Node, NullWritable, Text> {
		
		// Structure to store page-names and their pageranks
		private Map<String, Double> topKMap; 
		
		public void setup(Context context){
		
			// Initializing
			topKMap = new HashMap<String, Double>();
		}
		
		public void map(Text key, Node value, Context context) {
			
			// Put the page-name and its pagerank into the hashmap
			topKMap.put(key.toString(), value.getPageRank());
		}
		
		
		public void cleanup(Context context) throws IOException, InterruptedException {

			// Sort the entries in hashmap based on pagerank values
			List<Map.Entry<String, Double>> topKList = new LinkedList<Map.Entry<String, Double>>(topKMap.entrySet());
			Collections.sort(topKList, new Comparator() {
				public int compare(Object o1, Object o2) {
	                return ((Comparable) ((Map.Entry)(o2)).getValue()).compareTo(((Map.Entry)(o1)).getValue());
	            }
	        });
	       
			// Emit only the local top 100 highest ranked pages 
	        for (int i = 0; i < 100; i++) {
	        		context.write(NullWritable.get(), new Text(topKList.get(i).getKey() + " " + topKList.get(i).getValue()));
	        }
		}
		
	}
	
	
	/**
	 * Class TopKReducer
	 * Reducer receives local top 100 pages from all the mappers and computes global top 100
	 * highest ranked pages
	 *
	 */
	public static class TopKReducer extends Reducer<NullWritable, Text, NullWritable, Text> {
		
		public void reduce(NullWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
			
			// Initializing hashmap to store nid and pageranks
			Map<String, Double> topKMap = new HashMap<String, Double>();
			String pageName;
			Double pageRank;
			
			
			for (Text value : values) {
				String[] parts = value.toString().split(" ");
				pageName = parts[0];
				pageRank = Double.parseDouble(parts[1]);
				
				// Add the entry into the hashmap 
				topKMap.put(pageName, pageRank);
			}
	            
			
			// Sort the entries in hashmap based on pagerank values
			List<Map.Entry<String, Double>> topKList = new LinkedList<Map.Entry<String, Double>>(topKMap.entrySet());
			Collections.sort(topKList, new Comparator() {
	            public int compare(Object o1, Object o2) {
	                return ((Comparable) ((Map.Entry)(o2)).getValue()).compareTo(((Map.Entry)(o1)).getValue());
	            }
	        });
			
	        
	        Text pageRankOutput = new Text();
	        // Emit only the top 100 highest ranked pages 
	        for (int i = 0; i < 100; i++) {
	        		pageRankOutput.set(topKList.get(i).getKey() + ", " + topKList.get(i).getValue());
	        		context.write(NullWritable.get(), pageRankOutput);
	        }  
       
		}
		
	}
	
	
}
