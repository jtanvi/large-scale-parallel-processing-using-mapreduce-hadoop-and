package cs6240.hw03;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Counters;
//import org.apache.hadoop.mapred.Counters.Counter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;


public class PageRank {

	/**
	 * Class PageRankMapper
	 * Passes on the pagerank contributions of the given key for the outgoing links
	 * to the reducers. Also passes on the given key-value pair to the reducer 
	 *
	 */
	public static class PageRankMapper extends Mapper<Text, Node, Text, Node> {

		Double numOfPages;
		Double iterationNum;
		
		public void map(Text key, Node value, Context context) throws IOException, InterruptedException {
			
			// Extract current counts information from global counters
			Configuration conf = context.getConfiguration();
			iterationNum = Double.parseDouble(conf.get("ITERATION_NUM"));
			numOfPages = Double.parseDouble(conf.get("NUM_OF_PAGES"));
			Text neighborID = new Text();
			
			// If its the first iteration, pagerank should be 1 / (Total number of pages) 
			if (iterationNum == 1) {
				value.setPageRank(1.0 / (double) numOfPages);
			}
			
			// Passing along the graph structure
			context.write(key, value);
			
			// Compute contributions to send along outgoing links (if there are any)
			int listSize = value.getAdjList().size();
			if (listSize > 0) {
				Node neighborNode = new Node();
				double pageRank;
				pageRank = value.getPageRank() / (double) listSize;
				for (int i = 0; i < listSize; i++) {
					neighborID.set(value.getAdjList().get(i).toString());
					neighborNode.setNodeID(value.getAdjList().get(i));
					neighborNode.setPageRank(pageRank);
					neighborNode.setIsPGContribution(true);
					neighborNode.setAdjList(new ArrayList<String>());
					context.write(neighborID, neighborNode);
				}
			}		
		}	
	}
	
	
	/**
	 * Class PageRankReducer
	 * Reduce receives the node object for node m and the pagerank contributions for all m's inlinks
	 * 
	 * @author Tanvi
	 *
	 */
	public static class PageRankReducer extends Reducer<Text, Node, Text, Node> { 
			
			private Double alpha = 0.15;			// Probability factor for jumping to a random page
			Double numOfPages;
			Double dangNodesSum;
			double contribution;
			
			// Method to compute the constant term thats contributes to pagerank
			public void setup(Context context) throws IOException, InterruptedException {
				
				// Extract current counts information from global counters
				Configuration conf = context.getConfiguration();
				numOfPages = Double.parseDouble(conf.get("NUM_OF_PAGES"));
				dangNodesSum = Double.parseDouble(conf.get("DANGLINGNODES_CONTRIBUTION"));
				dangNodesSum = ((double)dangNodesSum) / Math.pow(10, 9);

				// compute constant term contribution
				contribution = ((alpha / numOfPages) + (1.0 - alpha) * (dangNodesSum / numOfPages));
			}
			
			public void reduce(Text key, Iterable<Node> values, Context context) throws IOException, InterruptedException {
				
				Double sum = 0.0;
				Double pageRank;
				ArrayList<String> newAdjList = new ArrayList<String>();
				
				for (Node node : values) {
					
					// A PageRank contribution from an inlink was found
					if (node.getIsPGContribution()) {
						sum += node.getPageRank();
					}
					else {
						// The node object was found: recover graph structure 
						newAdjList = node.getAdjList();
					}	
				}
				
				Node nNode = new Node();
				nNode.setNodeID(key.toString());
				// Compute pagerank for given page (key)
				pageRank = contribution + ((1.0-alpha) * sum);
				nNode.setPageRank(pageRank);
				nNode.setIsPGContribution(false);
				nNode.setAdjList(newAdjList);
				
				// If this page (key) is a dangling node, add its pagerank to delta counter
				if (newAdjList.size() == 0) {
		            context.getCounter(GlobalCounters.DANGLINGNODES_CONTRIBUTION).increment((long) (pageRank * Math.pow(10, 9)));
		        }
				
				// Add up all the pageranks for the given iteration
				context.getCounter(GlobalCounters.PAGERANK_SO_FAR).increment((long) (pageRank * Math.pow(10, 9)));
			
				// Emit nid n and its Node object N 
				context.write(key, nNode);
				
			}
		}
}
	
	

