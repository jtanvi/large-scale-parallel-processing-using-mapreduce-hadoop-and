INSTALLATION

- Import this folder in Eclipse ('File' -> 'Import' -> 'Existing Projects into
Workspace', Select this folder, 'Finish')
- The class files are present in Assignment1/src
- The program runs from the class named ‘Main’. In order to run the program, you need to provide a path to the data (.txt file) as an argument. You can do that under Run > Run Configurations.
- The running times as required by the problem will be displayed on the console window.

Additionally:
- The word count project has also been included in this repository 