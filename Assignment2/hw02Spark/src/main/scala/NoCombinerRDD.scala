import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD


object NoCombinerRDD {
  
  def main(args: Array[String]): Unit = {
    
    val conf = new SparkConf()
      .setAppName("Simple Application")
      .setMaster("local")
      
    val sc = new SparkContext(conf)
    
    if(args.length < 1) {
      println("Please provide args for input and output paths")
    }
      
    // Provide file-path as argument
    val data = sc.textFile(args(0))
    
    // Read the record as key-value pairs, key <- station ID and value <- (temp-type, temp-value)
    val pairs = data.map(x => (x.split(",")(0), (x.split(",")(2), x.split(",")(3).toDouble)))
    //pairs.foreach(println)
    
    
    // Computes average TMIN and TMAX for each station ID
    def compute_avg(inputList: List[(String, Double)]): ((String, Double), (String, Double)) = {
      //inputList(l)
      var maxSum = 0.0
      var minSum = 0.0
      var countMax = 0.0
      var countMin = 0.0
      var count = inputList.size
      
      for (i <- 0 until count) {
        val tempType = inputList(i)._1
        if (tempType == "TMIN") {
          minSum = minSum + inputList(i)._2
          countMin = countMin + 1
        }
        if (tempType == "TMAX") {
          maxSum = maxSum + inputList(i)._2
          countMax = countMax + 1
        }
      }
      
      // compute average TMIN and TMAX and return
      (("TMIN", (minSum / countMin)), ("TMAX", (maxSum / countMax))) 
    }
    
    // Apply "compute_avg" on resulting key-value pairs from groupByKey()
    val solpairs = pairs.groupByKey().map(pair => {
      (pair._1, compute_avg(pair._2.toList))
    })
    
    // Print the output records
    solpairs.foreach(println)  
    //solpairs.saveAsTextFile(args(1))
  }
}