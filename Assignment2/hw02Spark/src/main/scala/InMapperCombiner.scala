import org.apache.spark.SparkConf
import org.apache.spark.SparkContext


object InMapperCombiner {
  
  def main(args: Array[String]): Unit = {
    
    val conf = new SparkConf()
      .setAppName("Simple Application")
      .setMaster("local")
      
    val sc = new SparkContext(conf)
    
    if(args.length < 1) {
      println("Please provide args for input and output paths")
    }
      
    // Provide file-path as argument
    val data = sc.textFile(args(0))
      
    // Read the record as key-value pairs, key <- station ID and value <- (temp-type, temp-value)
    val pairs = data.map(x => (x.split(",")(0), (x.split(",")(2), x.split(",")(3).toDouble)))
    //pairs.foreach(println)
    
    // Change the structure of 'value': Instead of (temp-type, temp-value), store every record as
    // a tuple with 4 element -> (TMIN-temp, TMIN-count, TMAX-temp, TMAX-count)
    val rdd = pairs.mapValues(value => 
     if (value._1 == "TMIN")
       (1.0.toDouble, value._2.toDouble, 0.0.toDouble, 0.0.toDouble)
     else if (value._1 == "TMAX")
       (0.0.toDouble, 0.0.toDouble, 1.0.toDouble, value._2.toDouble))
     
     // Filter out all the empty records
     val solpairs = rdd.filter(record => record._2 != ())
     //solpairs.foreach(println)
     
     // Perform reduceByKey operation on values (by aggregating them as follows)
     val reduce_pairs = solpairs.reduceByKey{
      case ((x: Double, y: Double, a: Double, b: Double), (p: Double, q: Double, r: Double, s: Double)) =>
        if (r == 0.0) 
          (x+p, y+q, a, b)
        else if (p == 0.0)
          (x, y, a+r, b+s)
    }
    
    // Compute average 
    val new_pairs = reduce_pairs.mapValues {
      case (x: Double, y: Double, z: Double, w: Double) => (y/x, w/z)
    }
     
    // Print the output records
    new_pairs.foreach(println)  

  } 
}