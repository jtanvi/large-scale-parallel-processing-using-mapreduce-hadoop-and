import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import scala.collection.mutable.MutableList


object Combiner {
  
   def main(args: Array[String]): Unit = {
    
    val conf = new SparkConf()
      .setAppName("Simple Application")
      .setMaster("local")
      
    val sc = new SparkContext(conf)
    
    if(args.length < 1) {
      println("Please provide args for input and output paths")
    }
      
    // Provide file-path as argument
    val data = sc.textFile(args(0))
      
    // Read the record as key-value pairs, key <- station ID and value <- (temp-type, temp-value)
    val pairs = data.map(x => (x.split(",")(0), (x.split(",")(2), x.split(",")(3).toDouble)))
    //pairs.foreach(println)
    
    // Change the structure of 'value': Instead of (temp-type, temp-value), store every record as
    // a tuple with 4 element -> (TMIN-temp, TMIN-count, TMAX-temp, TMAX-count)
    val solpairs = pairs.mapValues(value => 
     if (value._1 == "TMIN")
       (1.0.toDouble, value._2.toDouble, 0.0.toDouble, 0.0.toDouble)
     else if (value._1 == "TMAX")
       (0.0.toDouble, 0.0.toDouble, 1.0.toDouble, value._2.toDouble))
     
     // Filter out all the empty records
     val rdd = solpairs.filter(record => record._2 != ())
     
     
     // To use aggregateByKey, we need 3 parameters:
     // 1. AN initial empty value or set; we'll add up the sum and counts from within and across partitions here
     var initialSet = (0.0.toDouble, 0.0.toDouble, 0.0.toDouble, 0.0.toDouble)
    
     // 2. A combining function that takes 2 parameters
     // Second parameter is merged into first here; the merging takes place within partition
     var addToSet = (acc: (Double, Double, Double, Double), value: (Double, Double, Double, Double)) => 
       if (value._3 == 0.0)
          ((acc._1+value._1).toDouble, (acc._2+value._2).toDouble, acc._3, acc._4)
        else if (value._1 == 0.0)
          (acc._1, acc._2, acc._3+value._3, acc._4+value._4)
        else 
           acc
     
     // 3. A merging function that takes 2 parameters which are merged into one
     // Merges values across partitions
     val mergePartitionSets = (acc1: (Double, Double, Double, Double), acc2: (Double, Double, Double, Double)) => 
       (acc1._1+ acc2._1, acc1._2+acc2._2, acc1._3+acc2._3, acc1._4+acc2._4)
          
     // Finally, calling the aggregateByKey function with above defined parameters
     val uniqueByKey = rdd.aggregateByKey(initialSet)(addToSet, mergePartitionSets)
     
     // computing the averages:
     val new_pairs = uniqueByKey.mapValues {
      case (x: Double, y: Double, z: Double, w: Double) => (y/x, w/z)
    }
     
     // Print the output records
     new_pairs.foreach(println)  
    
   }
}