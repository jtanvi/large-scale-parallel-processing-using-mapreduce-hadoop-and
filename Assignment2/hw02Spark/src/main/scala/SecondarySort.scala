import org.apache.spark.SparkConf
import org.apache.spark.SparkContext


object SecondarySort {
  
  def main(args: Array[String]): Unit = {
    
    val conf = new SparkConf()
      .setAppName("Simple Application")
      .setMaster("local")
      
    val sc = new SparkContext(conf)
    
    if(args.length < 1) {
      println("Please provide args for input and output paths")
    }
      
    // Provide file-path as argument
    val data = sc.textFile(args(0))
      
    // Read the record as key-value pairs, key <- station ID + year and value <- (temp-type, temp-value)
    val pairs = data.map(x => ((x.split(",")(0), x.split(",")(1).substring(0, 4)), (x.split(",")(2), x.split(",")(3).toDouble)))
    //pairs.foreach(println)
  }
}