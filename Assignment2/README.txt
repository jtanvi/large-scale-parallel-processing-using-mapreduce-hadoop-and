INSTRUCTIONS:

There are 2 makefiles present inside "hw02Hadoop" folder. One is for standalone and pseudo-cluster local run, and another one (makefile_aws) is for running on emr. Please change the paths in the makefiles for your environment.

The Map-reduce programs in Hadoop are in hw02Hadoop directory and the Spark-Scala programs are present in hw02Spark directory.