package cs6240.hw02;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableUtils;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import cs6240.hw02.Combiner.CombinerMapper;
import cs6240.hw02.Combiner.CombinerReducer;
import cs6240.hw02.Combiner.DataCombiner;
import cs6240.hw02.NoCombiner.ValueObject;

/*
 * Class Combiner
 * This program demonstrates a map-reduce computation of weather data using in-mapper
 * combiner technique that aggregates the values for a given key and stores them
 * in a hashmap in the mapper itself before emitting out the key-value pairs 
 */
public class InMapperCombiner {

	// Driver method for Combiner
	public static void main(String[] args) throws Exception {
		  
	    System.out.println("Starting InMapperCombiner...");
	    Configuration conf = new Configuration();
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length < 1) {
	      System.err.println("Provide args for input files and output path");
	      System.exit(2);
	    }
	    Job job = Job.getInstance(conf, "InMapperCombiner");
	    job.setJarByClass(InMapperCombiner.class);
	    
	    job.setMapperClass(CustomMapper.class);
	    job.setReducerClass(CustomReducer.class);
	    
	    job.setMapOutputKeyClass(Text.class);
	    job.setMapOutputValueClass(ValueObject.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Text.class);
	    for (int i = 0; i < otherArgs.length - 1; ++i) {
	      FileInputFormat.addInputPath(job, new Path(otherArgs[i]));
	    }
	    FileOutputFormat.setOutputPath(job,
	    new Path(otherArgs[otherArgs.length - 1]));
	    job.setNumReduceTasks(1);
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	  }


	/*
	 * Mapper class
	 * Here, we perform the aggregation of temperature values for a given key in the mapper 
	 * itself using a local data structure and then emit the key-value pairs
	 */
	public static class CustomMapper extends Mapper<Object, Text, Text, ValueObject>{
		 
		// Hash-Map to store stnID-year combination as Text and an array of double of 
		// size 4 to store min-Temperature, counts for records found for min-temperatures 
		// for the key and max-temperature and its corresponding counts
		Map<Text, double[]> minMaxTempValues;
		
		// Initializing the Hash-Map
		protected void setup(Context context) {
			minMaxTempValues = new HashMap<Text, double[]>();
		}
			
		
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException  {
			
			String dataRecord = value.toString();

			if (dataRecord.contains("TMIN") || dataRecord.contains("TMAX")) {
				
				// Create stnID-year combination key
				String[] str_values = dataRecord.split(",");
				String stnID = new String(str_values[0]);
				String year = new String(str_values[1].substring(0, 4));
				Text stnidYear = new Text(stnID + " " + year);
				
				Double currentTempValue = new Double(str_values[3]);
				String currentTempType = new String(str_values[2]);
				
				// Put the temperature values with stnID-year as key in the hashmap
				// if the key is not already present
				if (!minMaxTempValues.containsKey(stnidYear)) {
					if (currentTempType.equals("TMIN")) {
						double[] values = new double[4];
						values[0] = currentTempValue;
						values[1] = 1.0;
						values[2] = 0.0;
						values[3] = 0.0;
						minMaxTempValues.put(stnidYear, values);
					}
					else {
						double[] values = new double[4];
						values[0] = 0.0;
						values[1] = 0.0;
						values[2] = currentTempValue;
						values[3] = 1.0;
						minMaxTempValues.put(stnidYear, values);
					}	
				}
				
				// Else Add the new temperature found to the respective array element
				// depending upon whether the record is for TMAX or TMIN
				else {
					if (currentTempType.equals("TMIN")) {
						minMaxTempValues.put(stnidYear, new double[] {
							minMaxTempValues.get(stnidYear)[0] + currentTempValue,
							minMaxTempValues.get(stnidYear)[1] + 1.0,
							minMaxTempValues.get(stnidYear)[2],
							minMaxTempValues.get(stnidYear)[3]});
					}
					else {
						minMaxTempValues.put(stnidYear, new double[] {
								minMaxTempValues.get(stnidYear)[0],
								minMaxTempValues.get(stnidYear)[1],
								minMaxTempValues.get(stnidYear)[2] + currentTempValue,
								minMaxTempValues.get(stnidYear)[3] + 1.0});
					}
				}
				
			}
			
		}
	
		
		// Once all the key-value pairs are accumulated in the hash-map, we emit the records
		// one by one, in the form of key (stnId-year) and value (custom object storing temp-type,
		// temp-value and number of records of that type) pair.
		protected void cleanup(Context context) throws IOException, InterruptedException {
			
			for (Text key: minMaxTempValues.keySet()) {
				ValueObject valueObj = new ValueObject();
				valueObj.setTempType("TMIN");
				valueObj.setTempValue(minMaxTempValues.get(key)[0]);
				valueObj.setStnCount(minMaxTempValues.get(key)[1]);
				context.write(key, valueObj);
				
				valueObj.setTempType("TMAX");
				valueObj.setTempValue(minMaxTempValues.get(key)[2]);
				valueObj.setStnCount(minMaxTempValues.get(key)[3]);
				context.write(key, valueObj);
			}
			
		    
		}
	}

	/*
	 * Reducer class
	 * Computes average min and max temperatures from partly pre-computed aggregations of 
	 * temperature values in the mapper
	 */
	public static class CustomReducer extends Reducer<Text, ValueObject, Text, Text>{
		
		public void reduce(Text key, Iterable<ValueObject> values, Context context) throws IOException, InterruptedException {

			// Variables to keep track of running sum and count for both min-max temperatures
			double minAvg = 0.0;
			double maxAvg = 0.0;
			double sumTMAX = 0.0;
			double sumTMIN = 0.0;
			double countTMAX = 0.0;
			double countTMIN = 0.0;
			for (ValueObject valueObj : values) {
				
				if (valueObj.getTempType().equals("TMIN")) {
					countTMIN += valueObj.getStnCount(); 
					sumTMIN += valueObj.getTempValue();
				}
				else {
					countTMAX += valueObj.getStnCount();
					sumTMAX += valueObj.getTempValue();
				}
			}
			
			// Computing the averages
			minAvg = sumTMIN / countTMIN;
			maxAvg = sumTMAX / countTMAX;

			Text avgMinMaxTemp = new Text();
			avgMinMaxTemp.set(new String(minAvg + ", " + maxAvg));
			Text outputKey = new Text();
			outputKey.set(key.toString().substring(0, 11) + ",");
			context.write(outputKey, avgMinMaxTemp);
		}
	}
	
	
	/*
	 * Class ValueObject
	 * This is a custom writable class, defined to store multiple values 
	 * (temperature-type, temperature-value and station counts in this case) 
	 * per stationID-year key
	 */
	
	public static class ValueObject implements Writable{
		
		private String tempType = "";
		private Double tempValue = 0.0;
		private Double stnCount = 0.0;
		
		
		public void setStnCount(Double n) {
			this.stnCount = n;
		}
		
		public Double getStnCount() {
			return this.stnCount;
		}
		
		public void setTempType(String tempType) {
			this.tempType = tempType;
		}

		public void setTempValue(Double tempValue) {
			this.tempValue = tempValue;
		}

		public String getTempType() {
			return tempType;
		}


		public Double getTempValue() {
			return tempValue;
		}

		public void write(DataOutput out)throws IOException{
			WritableUtils.writeString(out, tempType);
			out.writeDouble(tempValue);
			out.writeDouble(stnCount);
		}
		
		public void readFields(DataInput in) throws IOException{
			tempType = WritableUtils.readString(in);
			tempValue = in.readDouble();
			stnCount = in.readDouble();
		}
		
		public static ValueObject read(DataInput in) throws IOException {
			ValueObject w = new ValueObject();
			w.readFields(in);
			return w;
		}
		
	}

	
}
