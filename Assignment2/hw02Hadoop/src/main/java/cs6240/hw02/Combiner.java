package cs6240.hw02;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableUtils;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

/*
 * Class Combiner
 * This program demonstrates a map-reduce computation of weather data using hadoop's
 * local combiner
 */

public class Combiner {
	
	// Driver method for Combiner
	public static void main(String[] args) throws Exception {
		  
	    System.out.println("Starting Combiner...");
	    Configuration conf = new Configuration();
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length < 1) {
	      System.err.println("Provide args for input files and output path");
	      System.exit(2);
	    }
	    Job job = Job.getInstance(conf, "Combiner");
	    job.setJarByClass(Combiner.class);
	    
	    job.setMapperClass(CombinerMapper.class);
	    job.setCombinerClass(DataCombiner.class);
	    job.setReducerClass(CombinerReducer.class);
	    
	    job.setMapOutputKeyClass(Text.class);
	    job.setMapOutputValueClass(ValueObject.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Text.class);
	    
	    for (int i = 0; i < otherArgs.length - 1; ++i) {
	      FileInputFormat.addInputPath(job, new Path(otherArgs[i]));
	    }
	    FileOutputFormat.setOutputPath(job,
	    new Path(otherArgs[otherArgs.length - 1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	  }
		
	
	/*
	 * Mapper Class
	 * Emits the stationID-year as key and a custom object consisting of temperature type 
	 * and temperature value for that key
	 */
	public static class CombinerMapper extends Mapper<Object, Text, Text, ValueObject>{
 
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException  {
			
			String dataRecord = value.toString();
			
			if (dataRecord.contains("TMIN") || dataRecord.contains("TMAX")){
			
				// Create stnID-year combination key
				String[] str_values = dataRecord.split(",");
				String stnID = new String(str_values[0]);
				String year = new String(str_values[1].substring(0, 4));
				Text stnidYear = new Text(stnID + " " + year);

				String tempType =  new String(str_values[2]);
				Double tempValue = new Double(Double.parseDouble(str_values[3]));
				
				// Add the temperature type and temperature value found in the custom object
				ValueObject valueObj = new ValueObject();
				valueObj.setTempType(tempType);
				valueObj.setTempValue(tempValue);
				valueObj.setStnCount(0.0);
				
				// Emit
				context.write(stnidYear, valueObj);
				
			}
		}
	}
	
	/*
	 * Combiner class
	 * The aggregation of min and max temperatures for a given key takes place here before
	 * the mapper outputs are sent to reducers
	 */
	public static class DataCombiner extends Reducer<Text, ValueObject, Text, ValueObject> {
		
		public void reduce(Text key, Iterable<ValueObject> values, Context context) throws IOException, InterruptedException {
			
			// Variables to keep track of running sum and count for both min-max temperatures
			double sumTMAX = 0.0;
			double sumTMIN = 0.0;
			double countTMAX = 0.0;
			double countTMIN = 0.0;
			
			for (ValueObject valueObj : values) {
				// Check if value is type - TMIN
				if (valueObj.getTempType().equals("TMIN")) {
					countTMIN += 1; 
					sumTMIN += valueObj.getTempValue();
				}
				else {
					countTMAX += 1; 
					sumTMAX += valueObj.getTempValue();
				}
			}
			
			// Creating new objects with aggregated sum and counts for each key
			ValueObject minValues = new ValueObject();
			minValues.setStnCount(countTMIN);
			minValues.setTempType("TMIN");
			minValues.setTempValue(sumTMIN);
			context.write(key, minValues);
			
			ValueObject maxValues = new ValueObject();
			maxValues.setStnCount(countTMAX);
			maxValues.setTempType("TMAX");
			maxValues.setTempValue(sumTMAX);
			context.write(key, maxValues);
		}
		
	}
	
	/*
	 * Reducer class
	 * Computes average min and max temperatures from partly pre-computed aggregations of 
	 * temperature values in Combiner
	 */
	public static class CombinerReducer extends Reducer<Text, ValueObject, Text, Text> {
		
		public void reduce(Text key, Iterable<ValueObject> values, Context context) throws IOException, InterruptedException {

			// Variables to keep track of running sum and count for both min-max temperatures
			double minAvg = 0.0;
			double maxAvg = 0.0;
			double sumTMAX = 0.0;
			double sumTMIN = 0.0;
			double countTMAX = 0.0;
			double countTMIN = 0.0;
			
			for (ValueObject valueObj : values) {
				if (valueObj.getTempType().equals("TMIN")) {
					// Here, instead of incrementing as in NoCombiner, we add up the station counts
					countTMIN += valueObj.getStnCount(); 
					sumTMIN += valueObj.getTempValue();
				}
				else {
					countTMAX += valueObj.getStnCount();
					sumTMAX += valueObj.getTempValue();
				}
			}
			
			// Computing the averages
			minAvg = sumTMIN / countTMIN;
			maxAvg = sumTMAX / countTMAX;

			Text avgMinMaxTemp = new Text();
			avgMinMaxTemp.set(new String(minAvg + ", " + maxAvg));
			Text outputKey = new Text();
			outputKey.set(key.toString().substring(0, 11) + ",");
			context.write(outputKey, avgMinMaxTemp);
		}
	}
	
	
	
	/*
	 * Class ValueObject
	 * This is a custom writable class, defined to store multiple values 
	 * (temperature-type, temperature-value and station counts in this case) 
	 * per stationID-year key
	 */
	
	public static class ValueObject implements Writable{
		
		private String tempType;
		private Double tempValue;
		private Double stnCount;
		
		public void setStnCount(Double n) {
			this.stnCount = n;
		}
		
		public Double getStnCount() {
			return this.stnCount;
		}
		
		public void setTempType(String tempType) {
			this.tempType = tempType;
		}

		public void setTempValue(Double tempValue) {
			this.tempValue = tempValue;
		}

		public String getTempType() {
			return tempType;
		}


		public Double getTempValue() {
			return tempValue;
		}

		public void write(DataOutput out)throws IOException{
			WritableUtils.writeString(out, tempType);
			out.writeDouble(tempValue);
			out.writeDouble(stnCount);
		}
		
		public void readFields(DataInput in) throws IOException{
			tempType = WritableUtils.readString(in);
			tempValue = in.readDouble();
			stnCount = in.readDouble();
		}
		
		public static ValueObject read(DataInput in) throws IOException {
			ValueObject w = new ValueObject();
			w.readFields(in);
			return w;
		}
	}
}
