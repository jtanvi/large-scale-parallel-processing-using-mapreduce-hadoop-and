package cs6240.hw02;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.io.WritableUtils;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import cs6240.hw02.Combiner.CombinerMapper;
import cs6240.hw02.Combiner.CombinerReducer;
import cs6240.hw02.Combiner.DataCombiner;
import cs6240.hw02.InMapperCombiner.ValueObject;


/*
 * Class SecondarySort
 * This program demonstrates a map-reduce computation of weather data using secondary sort
 * that sort the records by station ID and year and aggregates the corresponding values 
 * before emitting them
 */
public class SecondarySort {
	

	// Driver method for Combiner
	public static void main(String[] args) throws Exception {
		  
	    System.out.println("Starting Secondary Sort...");
	    Configuration conf = new Configuration();
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length < 1) {
	      System.err.println("Provide args for input files and output path");
	      System.exit(2);
	    }
	    Job job = Job.getInstance(conf, "SecondarySort");
	    job.setJarByClass(SecondarySort.class);
	    
	    job.setMapperClass(SSMapper.class);
	    job.setSortComparatorClass(SSKeyComparator.class);
	    job.setPartitionerClass(SSKeyPartitioner.class);
	    job.setGroupingComparatorClass(SSGroupingComparator.class);
	    job.setCombinerClass(DataCombiner.class);
	    job.setReducerClass(SSReducer.class);
	    
	    job.setMapOutputKeyClass(Text.class);
	    job.setMapOutputValueClass(ValueObject.class);
	    
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Text.class);
	    
	    for (int i = 0; i < otherArgs.length - 1; ++i) {
	      FileInputFormat.addInputPath(job, new Path(otherArgs[i]));
	    }
	    FileOutputFormat.setOutputPath(job,
	    new Path(otherArgs[otherArgs.length - 1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	  }
	
	
	
	/*
	 * Mapper Class
	 * Emits the stationID-year as key and a custom object consisting of temperature type, 
	 * temperature value, station count (default = 1) and year for that key
	 */
	public static class SSMapper extends Mapper<Object, Text, Text, ValueObject> {
		
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException  {
			
			String dataRecord = value.toString();
			
			if (dataRecord.contains("TMIN") || dataRecord.contains("TMAX")){
			
				// Create stnID-year combination key
				String[] str_values = dataRecord.split(",");
				String stnID = new String(str_values[0]);
				String year = new String(str_values[1].substring(0, 4));
				Text stnidYear = new Text(stnID + " " + year);
			
				String tempType =  new String(str_values[2]);
				Double tempValue = new Double(Double.parseDouble(str_values[3]));
				
				// Add the temperature type, temperature value, default count and year found in the custom object
				ValueObject valueObj = new ValueObject();
				valueObj.setYear(year);
				valueObj.setTempType(tempType);
				valueObj.setTempValue(tempValue);
				valueObj.setStnCount(1.0);

				// Emit
				context.write(stnidYear, valueObj);
				
			}
		}
	}
	
	/*
	 * Class SSKeyComparator
	 * Custom class that extends WritableComparator, here we compare both the 
	 * natural key (station ID) and natural value (year) in order to sort the records
	 */
	public static class SSKeyComparator extends WritableComparator {
		
		protected SSKeyComparator() {
			super(Text.class, true);
		}
		
		public int compare(WritableComparable key1, WritableComparable key2){
			String[] keys1 = ((Text) key1).toString().split(" ");
			String[] keys2 = ((Text) key2).toString().split(" ");
			
			String stn1 = keys1[0];
			String year1 = keys1[1];
			String stn2 = keys2[0];
			String year2 = keys2[1];
			
			// sort the keys by station-ID first
			// first station IDs are same, sort them by year
			int stnCmp = stn1.compareTo(stn2);
			if (stnCmp == 0)
				return year1.compareTo(year2);
			else
				return stnCmp;
		}
	}
		
		
	/*
	 * Class SSKeyPartitioner
	 * Custom class that extends Partitioner required to passing records to Reducers in a 
	 * sorted order. The keys are hashed based on stationID
	 */
	public static class SSKeyPartitioner extends Partitioner<Text, ValueObject> {

		@Override
		public int getPartition(Text key, ValueObject value, int numPartitions) {
			// TODO Auto-generated method stub
			return Math.abs(key.toString().split(" ")[0].hashCode() % numPartitions);
		}
		
	}
	
	
	/*
	 * Class SSGroupingComparator
	 * Custom class that extends Partitioner, helps in passing records with same station ID 
	 * (already sorted by both station-ID and value) to a single Reduce call
	 */
	public static class SSGroupingComparator extends WritableComparator {
		
		protected SSGroupingComparator(){
			super(Text.class,true);
		}
		
		public int compare(WritableComparable key1, WritableComparable key2){
			String stn1 = ((Text) key1).toString().split(" ")[0];
			String stn2 = ((Text) key2).toString().split(" ")[0];
			
			// Sort only based on station ID 
			return stn1.compareTo(stn2);
		}
	}
	

	/*
	 * Class DataCombiner
	 * Combiner class that combines the records and computes aggregate TMIN and TMAX temperatures
	 * for a given key (station-ID and year combination)
	 */
	public static class DataCombiner extends Reducer<Text, ValueObject, Text, ValueObject> {
		
		public void reduce(Text key, Iterable<ValueObject> values, Context context) throws IOException, InterruptedException {
			
			// Variables to keep track of running sum and count for both min-max temperatures
			double sumTMAX = 0.0;
			double sumTMIN = 0.0;
			double countTMAX = 0.0;
			double countTMIN = 0.0;

			// Extract year from key
			String year = key.toString().split(" ")[1];
			
			for (ValueObject valueObj : values) {
				// Check if value is type - TMIN
				if (valueObj.getTempType().equals("TMIN")) {
					countTMIN += 1; 
					sumTMIN += valueObj.getTempValue();
				}
				else {
					countTMAX += 1; 
					sumTMAX += valueObj.getTempValue();
				}
			}
			
			// Creating new objects with aggregated sum, counts for each key
			ValueObject minValues = new ValueObject();
			minValues.setStnCount(countTMIN);
			minValues.setTempType("TMIN");
			minValues.setTempValue(sumTMIN);
			minValues.setYear(year);
			context.write(key, minValues);
			
			ValueObject maxValues = new ValueObject();
			maxValues.setStnCount(countTMAX);
			maxValues.setTempType("TMAX");
			maxValues.setTempValue(sumTMAX);
			maxValues.setYear(year);
			context.write(key, maxValues);
		}
	}
	


	/*
	 * Class SSReducer
	 * Computes average min and max temperatures from partly pre-computed aggregations of 
	 * temperature values obtained from Combiner
	 */
	public static class SSReducer extends Reducer<Text, ValueObject, Text, Text> {
		
		// Here, one reduce call gets (station-ID-*) as key (* being any year) and list of values sorted by year for that
		// station-ID. Hence the computation for records from all years obtained for the given station-ID takes place
		// in a single reduce call
		public void reduce(Text key, Iterable<ValueObject> values, Context context) throws IOException, InterruptedException {
			
			// Initialize a linked-hashmap to store the running sum and counts for TMIN and TMAX with year as the key
			Map<String, double[]> tempPerYear = new LinkedHashMap<String, double[]>();
			
			for (ValueObject valueObj : values) {
				String year = valueObj.getYear();
				
				// If the records for the given year isn't present in hashmap, add them
				if (!tempPerYear.containsKey(valueObj.getYear())) {

					// If the temp-type is "TMIN"
					if (valueObj.getTempType().equals("TMIN")) {
						double[] minValues = new double[4];
						minValues[0] = valueObj.getTempValue();
						minValues[1] = valueObj.getStnCount();
						minValues[2] = 0.0;
						minValues[3] = 0.0;
						tempPerYear.put(valueObj.getYear(), minValues);
					}
					else {   // If the temp-type is "TMAX"
						double[] maxValues = new double[4];
						maxValues[0] = 0.0;
						maxValues[1] = 0.0;
						maxValues[2] = valueObj.getTempValue();
						maxValues[3] = valueObj.getStnCount();
						tempPerYear.put(valueObj.getYear(), maxValues);
					}
				}
				else {
					// If the temp-type is "TMIN"
					if (valueObj.getTempType().equals("TMIN")) {
						tempPerYear.put(valueObj.getYear(), new double[] {
							tempPerYear.get(year)[0] + valueObj.getTempValue(),
							tempPerYear.get(year)[1] + valueObj.getStnCount(),
							tempPerYear.get(year)[2],
							tempPerYear.get(year)[3]
						});		
					}
					else {   // If the temp-type is "TMAX"
						tempPerYear.put(valueObj.getYear(), new double[] {
							tempPerYear.get(year)[0],
							tempPerYear.get(year)[1],
							tempPerYear.get(year)[2] + valueObj.getTempValue(),
							tempPerYear.get(year)[3] + valueObj.getStnCount()
						});
					}
				}
				
			}
			
			double minAvg;
			double maxAvg;

			// Create string that needs to emit
			String yearValues = "[";
			for (String stnKey: tempPerYear.keySet()) {

				// Compute average for that year of data
				minAvg = tempPerYear.get(stnKey)[0] / tempPerYear.get(stnKey)[1];
				maxAvg = tempPerYear.get(stnKey)[2] / tempPerYear.get(stnKey)[3];
				yearValues = yearValues + "(" + stnKey + ", " + String.format("%.2f", minAvg) + ", " + String.format("%.2f", maxAvg) + "),";
			}
			yearValues = yearValues + "]";

			Text avgMinMaxTemp = new Text();
			avgMinMaxTemp.set(yearValues.toString());
			Text outputKey = new Text();
			outputKey.set(key.toString().substring(0, 11) + ",");
			context.write(outputKey, avgMinMaxTemp);
		}
	}
	
	
	

	/*
	 * Class ValueObject
	 * This is a custom writable class, defined to store multiple values 
	 * (temperature-type, temperature-value, station counts and year in this case) 
	 * per stationID-year key
	 */

	public static class ValueObject implements Writable{
		
		private String tempType = "";
		private Double tempValue = 0.0;
		private Double stnCount = 0.0;
		private String year;
		
		public void setYear(String year) {
			this.year = year;
		}
		
		public String getYear() {
			return this.year;
		}
		
		public void setStnCount(Double n) {
			this.stnCount = n;
		}
		
		public Double getStnCount() {
			return this.stnCount;
		}
		
		public void setTempType(String tempType) {
			this.tempType = tempType;
		}

		public void setTempValue(Double tempValue) {
			this.tempValue = tempValue;
		}

		public String getTempType() {
			return tempType;
		}


		public Double getTempValue() {
			return tempValue;
		}

		public void write(DataOutput out)throws IOException{
			WritableUtils.writeString(out, tempType);
			out.writeDouble(tempValue);
			out.writeDouble(stnCount);
			WritableUtils.writeString(out, year);
		}
		
		public void readFields(DataInput in) throws IOException{
			tempType = WritableUtils.readString(in);
			tempValue = in.readDouble();
			stnCount = in.readDouble();
			year = WritableUtils.readString(in);
		}
		
		public static ValueObject read(DataInput in) throws IOException {
			ValueObject w = new ValueObject();
			w.readFields(in);
			return w;
		}
		
	}
	
}
