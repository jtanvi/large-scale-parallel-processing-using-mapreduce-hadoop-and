package cs6240.hw02;
import java.util.ArrayList;
import java.util.List;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableUtils;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

/*
 * Class NoCombiner
 * This program demonstrates a map-reduce computation on ghcn weather data without the use 
 * a combiner
 */
public class NoCombiner {
	
	// Driver method for NoCombiner
	public static void main(String[] args) throws Exception {
		  
	    System.out.println("Starting No Combiner...");
	    Configuration conf = new Configuration();
	    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
	    if (otherArgs.length < 1) {
	      System.err.println("Provide args for input files and output path");
	      System.exit(2);
	    }
	    Job job = Job.getInstance(conf, "NoCombiner");
	    job.setJarByClass(NoCombiner.class);
	    
	    job.setMapperClass(NoCombinerMapper.class);
	    job.setReducerClass(NoCombinerReducer.class);
	    
	    job.setMapOutputKeyClass(Text.class);
	    job.setMapOutputValueClass(ValueObject.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Text.class);
	    
	    for (int i = 0; i < otherArgs.length - 1; ++i) {
	      FileInputFormat.addInputPath(job, new Path(otherArgs[i]));
	    }
	    FileOutputFormat.setOutputPath(job,
	    new Path(otherArgs[otherArgs.length - 1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	  }
	
	
	
/*
 * Mapper Class
 * Emits the stationID-year as key and a custom object consisting of temperature type and temperature value
 * for that key
 */
	public static class NoCombinerMapper extends Mapper<Object, Text, Text, ValueObject>{
 
			public void map(Object key, Text value, Context context) throws IOException, InterruptedException  {
				
				String dataRecord = value.toString();
				if (dataRecord.contains("TMIN") || dataRecord.contains("TMAX")){
				
					String[] str_values = dataRecord.split(",");
					
					// Create stnID-year combination key
					String stnID = new String(str_values[0]);
					String year = new String(str_values[1].substring(0, 4));
					Text stnidYear = new Text(stnID + " " + year);
				
					String tempType =  new String(str_values[2]);
					Double tempValue = new Double(Double.parseDouble(str_values[3]));
					
					// Add the temperature type and temperature value found in the custom object
					ValueObject valueObj = new ValueObject();
					valueObj.setTempType(tempType);
					valueObj.setTempValue(tempValue);
					
					// Emit
					context.write(stnidYear, valueObj);			
				}
			}
	}
	
	/*
	 * Reducer Class
	 * Computes the average of all minimum and maximum temperature found for a given key
	 */
	
	public static class NoCombinerReducer extends Reducer<Text, ValueObject, Text, Text> {
		
		// Each reduce call gets a unique key and a list of corresponding values
		public void reduce(Text key, Iterable<ValueObject> values, Context context) throws IOException, InterruptedException {
			
			// Variables to keep track of running sum and count for both min-max temperatures
			double minCount = 0.0;
			double maxCount = 0.0;
			double minSum = 0.0;
			double maxSum = 0.0;
			
			for (ValueObject valueObj: values){
				// Check if value is type - TMIN
				if (valueObj.getTempType().equals("TMIN")) {
					minCount += 1;
					minSum += valueObj.getTempValue();
				}
				else {
					maxCount += 1;
					maxSum += valueObj.getTempValue();
				}
				
			}
			
			// Computing the averages
			double minAvg = minSum / minCount;
			double maxAvg = maxSum / maxCount;
			
			Text avgMinMaxTemp = new Text();
			avgMinMaxTemp.set(new String(minAvg + ", " + maxAvg));
			Text outputKey = new Text();
			outputKey.set(key.toString().substring(0, 11) + ",");
			context.write(outputKey, avgMinMaxTemp);
		}
	}
	
	
	/*
	 * Class ValueObject
	 * This is a custom writable class, defined to store multiple values 
	 * (temperature-type and temperature-value in this case) per stationID-year key
	 */
	
	public static class ValueObject implements Writable{
		
		private String tempType;
		private Double tempValue;
		
		public void setTempType(String tempType) {
			this.tempType = tempType;
		}

		public void setTempValue(Double tempValue) {
			this.tempValue = tempValue;
		}

		public String getTempType() {
			return tempType;
		}

		public Double getTempValue() {
			return tempValue;
		}

		public void write(DataOutput out)throws IOException{
			WritableUtils.writeString(out, tempType);
			out.writeDouble(tempValue);
		}
		
		public void readFields(DataInput in) throws IOException{
			tempType = WritableUtils.readString(in);
			tempValue = in.readDouble();
		}
		
		public static ValueObject read(DataInput in) throws IOException {
			ValueObject w = new ValueObject();
			w.readFields(in);
			return w;
		}
		
	}
}
