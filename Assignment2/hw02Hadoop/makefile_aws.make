# Customize these paths for your environment.
# -----------------------------------------------------------
hadoop.root=hadoop
local.input=input

# Jar and Job parameters
jar.name=hw02-0.0.1-SNAPSHOT.jar
jar.path=target/${jar.name}
job.nocombiner.name=cs6240.hw02.NoCombiner
job.combiner.name=cs6240.hw02.Combiner
job.inmapper.name=cs6240.hw02.InMapperCombiner
job.secondarysort.name=cs6240.hw02.SecondarySort

# Output parameters
output.nocombiner=output_nocombiner_aws
output.combiner=output_combiner_aws
output.inmapper=output_inmapper_aws
output.secondarysort=output_secondarysort_aws

# Pseudo-Cluster Execution
hdfs.user.name=tanvi
hdfs.input=input
local.output=output_aws


# AWS EMR Execution
aws.emr.release=emr-5.2.1
aws.region=us-east-1
aws.bucket.name=cs6240-tanvi
aws.subnet.id=subnet-6356553a
aws.input=input
aws.output.nocombiner=output_nocombiner
aws.output.combiner=output_combiner
aws.output.inmapper=output_inmapper
aws.output.secondarysort=output_secondarysort
aws.log.dir=log
aws.num.nodes=6
aws.instance.type=m4.large


# Removes local output directory.
clean-local-output:
	rm -rf ${output.nocombiner}*
	rm -rf ${output.combiner}*
	rm -rf ${output.inmapper}*
	rm -rf ${output.secondarysort}*

# Create S3 bucket.
make-bucket:
	aws s3 mb s3://${aws.bucket.name}

# Upload data to S3 input dir.
upload-input-aws: make-bucket
	aws s3 sync ${local.input} s3://${aws.bucket.name}/${aws.input}
	
# Delete S3 output dir.
delete-output-aws:
	aws s3 rm s3://${aws.bucket.name}/ --recursive --exclude "*" --include "${aws.output.nocombiner}*"
	aws s3 rm s3://${aws.bucket.name}/ --recursive --exclude "*" --include "${aws.output.combiner}*"
	aws s3 rm s3://${aws.bucket.name}/ --recursive --exclude "*" --include "${aws.output.inmapper}*"
	aws s3 rm s3://${aws.bucket.name}/ --recursive --exclude "*" --include "${aws.output.secondarysort}*"

# Upload application to S3 bucket.
upload-app-aws:
	aws s3 cp ${jar.path} s3://${aws.bucket.name}

# Main EMR launch.
cloud: jar upload-app-aws delete-output-aws
	aws emr create-cluster \
		--name "NoCombiner Cluster" \
		--release-label ${aws.emr.release} \
		--instance-groups '[{"InstanceCount":${aws.num.nodes},"InstanceGroupType":"CORE","InstanceType":"${aws.instance.type}"},{"InstanceCount":1,"InstanceGroupType":"MASTER","InstanceType":"${aws.instance.type}"}]' \
	    --applications Name=Hadoop \
	    --steps '[{"Args":["${job.nocombiner.name}","s3://${aws.bucket.name}/${aws.input}","s3://${aws.bucket.name}/${aws.output.nocombiner}"],"Type":"CUSTOM_JAR","Jar":"s3://${aws.bucket.name}/${jar.name}","ActionOnFailure":"TERMINATE_CLUSTER","Name":"Custom JAR"}]' \
		--log-uri s3://${aws.bucket.name}/${aws.log.dir} \
		--service-role EMR_DefaultRole \
		--ec2-attributes InstanceProfile=EMR_EC2_DefaultRole,SubnetId=${aws.subnet.id} \
		--region ${aws.region} \
		--enable-debugging \
		--auto-terminate

	aws emr create-cluster \
		--name "Combiner Cluster" \
		--release-label ${aws.emr.release} \
		--instance-groups '[{"InstanceCount":${aws.num.nodes},"InstanceGroupType":"CORE","InstanceType":"${aws.instance.type}"},{"InstanceCount":1,"InstanceGroupType":"MASTER","InstanceType":"${aws.instance.type}"}]' \
	    --applications Name=Hadoop \
	    --steps '[{"Args":["${job.combiner.name}","s3://${aws.bucket.name}/${aws.input}","s3://${aws.bucket.name}/${aws.output.combiner}"],"Type":"CUSTOM_JAR","Jar":"s3://${aws.bucket.name}/${jar.name}","ActionOnFailure":"TERMINATE_CLUSTER","Name":"Custom JAR"}]' \
		--log-uri s3://${aws.bucket.name}/${aws.log.dir} \
		--service-role EMR_DefaultRole \
		--ec2-attributes InstanceProfile=EMR_EC2_DefaultRole,SubnetId=${aws.subnet.id} \
		--region ${aws.region} \
		--enable-debugging \
		--auto-terminate

	aws emr create-cluster \
		--name "InMapperCombiner Cluster" \
		--release-label ${aws.emr.release} \
		--instance-groups '[{"InstanceCount":${aws.num.nodes},"InstanceGroupType":"CORE","InstanceType":"${aws.instance.type}"},{"InstanceCount":1,"InstanceGroupType":"MASTER","InstanceType":"${aws.instance.type}"}]' \
	    --applications Name=Hadoop \
	    --steps '[{"Args":["${job.inmapper.name}","s3://${aws.bucket.name}/${aws.input}","s3://${aws.bucket.name}/${aws.output.inmapper}"],"Type":"CUSTOM_JAR","Jar":"s3://${aws.bucket.name}/${jar.name}","ActionOnFailure":"TERMINATE_CLUSTER","Name":"Custom JAR"}]' \
		--log-uri s3://${aws.bucket.name}/${aws.log.dir} \
		--service-role EMR_DefaultRole \
		--ec2-attributes InstanceProfile=EMR_EC2_DefaultRole,SubnetId=${aws.subnet.id} \
		--region ${aws.region} \
		--enable-debugging \
		--auto-terminate

	aws emr create-cluster \
		--name "SecondarySort Cluster" \
		--release-label ${aws.emr.release} \
		--instance-groups '[{"InstanceCount":${aws.num.nodes},"InstanceGroupType":"CORE","InstanceType":"${aws.instance.type}"},{"InstanceCount":1,"InstanceGroupType":"MASTER","InstanceType":"${aws.instance.type}"}]' \
	    --applications Name=Hadoop \
	    --steps '[{"Args":["${job.secondarysort.name}","s3://${aws.bucket.name}/${aws.input}","s3://${aws.bucket.name}/${aws.output.secondarysort}"],"Type":"CUSTOM_JAR","Jar":"s3://${aws.bucket.name}/${jar.name}","ActionOnFailure":"TERMINATE_CLUSTER","Name":"Custom JAR"}]' \
		--log-uri s3://${aws.bucket.name}/${aws.log.dir} \
		--service-role EMR_DefaultRole \
		--ec2-attributes InstanceProfile=EMR_EC2_DefaultRole,SubnetId=${aws.subnet.id} \
		--region ${aws.region} \
		--enable-debugging \
		--auto-terminate

# Download output from S3.
download-output-aws: clean-local-output
	mkdir ${output.nocombiner}
	mkdir ${output.combiner}
	mkdir ${output.inmapper}
	mkdir ${output.secondarysort}
	aws s3 sync s3://${aws.bucket.name}/${aws.output.nocombiner} ${output.nocombiner}
	aws s3 sync s3://${aws.bucket.name}/${aws.output.combiner} ${output.combiner}
	aws s3 sync s3://${aws.bucket.name}/${aws.output.inmapper} ${output.inmapper}
	aws s3 sync s3://${aws.bucket.name}/${aws.output.secondarysort} ${output.secondarysort}

# Change to standalone mode.
switch-standalone:
	cp config/standalone/*.xml ${hadoop.root}/etc/hadoop

# Change to pseudo-cluster mode.
switch-pseudo:
	cp config/pseudo/*.xml ${hadoop.root}/etc/hadoop

# Package for release.
distro:
	rm -rf build
	mkdir build
	mkdir build/deliv
	mkdir build/deliv/Homework02
	cp pom.xml build/deliv/Homework02
	cp -r src build/deliv/Homework02
	cp Makefile build/deliv/Homework02
	cp README.txt build/deliv/Homework02
	tar -czf Homework02.tar.gz -C build/deliv Homework02
	cd build/deliv && zip -rq ../../Homework02.zip Homework02




	