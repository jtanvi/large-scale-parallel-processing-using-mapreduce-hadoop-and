# Customize these paths for your environment.
# -----------------------------------------------------------
hadoop.root=hadoop
local.input=input

# Jar and Job parameters
jar.name=hw02-0.0.1-SNAPSHOT.jar
jar.path=target/${jar.name}
job.nocombiner.name=cs6240.hw02.NoCombiner
job.combiner.name=cs6240.hw02.Combiner
job.inmapper.name=cs6240.hw02.InMapperCombiner
job.secondarysort.name=cs6240.hw02.SecondarySort

# Output parameters
output.nocombiner=output_nocombiner
output.combiner=output_combiner
output.inmapper=output_inmapper
output.secondarysort=output_secondarysort

# Pseudo-Cluster Execution
hdfs.user.name=tanvi
hdfs.input=input
local.output=output_from_hdfs

# Compiles code and builds jar (with dependencies).
jar:
	mvn clean compile package

# Removes local output directory.
clean-local-output:
	rm -rf ${local.output}*
	rm -rf ${output.nocombiner}*
	rm -rf ${output.combiner}*
	rm -rf ${output.inmapper}*
	rm -rf ${output.secondarysort}*

# Runs standalone
# Make sure Hadoop  is set up (in /etc/hadoop files) for standalone operation (not pseudo-cluster).
# https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/SingleCluster.html#Standalone_Operation
alone: jar clean-local-output
	${hadoop.root}/bin/hadoop jar ${jar.path} ${job.nocombiner.name} ${local.input} ${output.nocombiner}
	${hadoop.root}/bin/hadoop jar ${jar.path} ${job.combiner.name} ${local.input} ${output.combiner}
	${hadoop.root}/bin/hadoop jar ${jar.path} ${job.inmapper.name} ${local.input} ${output.inmapper}
	${hadoop.root}/bin/hadoop jar ${jar.path} ${job.secondarysort.name} ${local.input} ${output.secondarysort}



# Start HDFS
start-hdfs:
	${hadoop.root}/sbin/start-dfs.sh

# Stop HDFS
stop-hdfs: 
	${hadoop.root}/sbin/stop-dfs.sh
	
# Start YARN
start-yarn: stop-yarn
	${hadoop.root}/sbin/start-yarn.sh

# Stop YARN
stop-yarn:
	${hadoop.root}/sbin/stop-yarn.sh

# Reformats & initializes HDFS.
format-hdfs: stop-hdfs
	rm -rf /tmp/hadoop*
	${hadoop.root}/bin/hdfs namenode -format

# Initializes user & input directories of HDFS.	
init-hdfs: start-hdfs
	${hadoop.root}/bin/hdfs dfs -rm -r -f /user
	${hadoop.root}/bin/hdfs dfs -mkdir /user
	${hadoop.root}/bin/hdfs dfs -mkdir /user/${hdfs.user.name}
	${hadoop.root}/bin/hdfs dfs -mkdir /user/${hdfs.user.name}/${hdfs.input}

# Load data to HDFS
upload-input-hdfs: start-hdfs
	${hadoop.root}/bin/hdfs dfs -put ${local.input}/* /user/${hdfs.user.name}/${hdfs.input}

# Removes hdfs output directory.
clean-hdfs-output:
	${hadoop.root}/bin/hdfs dfs -rm -r -f ${output.nocombiner}*
	${hadoop.root}/bin/hdfs dfs -rm -r -f ${output.combiner}*
	${hadoop.root}/bin/hdfs dfs -rm -r -f ${output.inmapper}*
	${hadoop.root}/bin/hdfs dfs -rm -r -f ${output.secondarysort}*

# Download output from HDFS to local.
download-output:
	mkdir ${local.output}
	${hadoop.root}/bin/hdfs dfs -get ${output.nocombiner}/* ${local.output}
	${hadoop.root}/bin/hdfs dfs -get ${output.combiner}/* ${local.output}
	${hadoop.root}/bin/hdfs dfs -get ${output.inmapper}/* ${local.output}
	${hadoop.root}/bin/hdfs dfs -get ${output.secondarysort}/* ${local.output}

# Runs pseudo-clustered (ALL). ONLY RUN THIS ONCE, THEN USE: make pseudoq
# Make sure Hadoop  is set up (in /etc/hadoop files) for pseudo-clustered operation (not standalone).
# https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/SingleCluster.html#Pseudo-Distributed_Operation
pseudo: jar stop-yarn format-hdfs init-hdfs upload-input-hdfs start-yarn clean-local-output 
	${hadoop.root}/bin/hadoop jar ${jar.path} ${job.nocombiner.name} ${hdfs.input} ${output.nocombiner}
	${hadoop.root}/bin/hadoop jar ${jar.path} ${job.combiner.name} ${hdfs.input} ${output.combiner}
	${hadoop.root}/bin/hadoop jar ${jar.path} ${job.inmapper.name} ${hdfs.input} ${output.inmapper}
	${hadoop.root}/bin/hadoop jar ${jar.path} ${job.secondarysort.name} ${hdfs.input} ${output.secondarysort}
	make download-output


# Runs pseudo-clustered (quickie).
pseudoq: jar clean-local-output clean-hdfs-output 
	${hadoop.root}/bin/hadoop jar ${jar.path} ${job.nocombiner.name} ${hdfs.input} ${output.nocombiner}
	${hadoop.root}/bin/hadoop jar ${jar.path} ${job.combiner.name} ${hdfs.input} ${output.combiner}
	${hadoop.root}/bin/hadoop jar ${jar.path} ${job.inmapper.name} ${hdfs.input} ${output.inmapper}
	${hadoop.root}/bin/hadoop jar ${jar.path} ${job.secondarysort.name} ${hdfs.input} ${output.secondarysort}
	make download-output


