import java.util.Collections;
import java.util.List;
import java.util.Map;

/*
 * Class Auxiliary
 * A Helper class defined to have auxiliary functions required by the program
 */
public class Auxiliary {
	
	/*
	 * Function that computes fibonacci, given a number n
	 */
	public static int executeFibonacci(int n) {
		if(n == 0 || n == 1) 
			return 1;
		
		return executeFibonacci(n-1) + executeFibonacci(n-2);
	}

	/*
	 * Function that displays the minimum, average and maximum time taken for a computation
	 */
	public static void displayObservedTimes(List<Double> timeRecords) {
		
		Double avgTime = (timeRecords.stream().mapToDouble(val -> val).average().getAsDouble());
		System.out.println("Min Time: " + Collections.min(timeRecords));
		System.out.println("Average time taken: " + avgTime);
		System.out.println("Max Time: " + Collections.max(timeRecords));
	}
	
	/*
	 * Function that displays the average TMAX temperature per station ID in the 
	 * given accumulation data structure
	 */
	public static void displayTemps(Map<String, double[]> accumulationDS) {
		for (Map.Entry<String,double[]> entry : accumulationDS.entrySet()) {
			  String key = entry.getKey();
			  double[] values = entry.getValue();
			  System.out.println("Key: " + key + "; Temp: " + values[0]);
		}
		
	}

}
