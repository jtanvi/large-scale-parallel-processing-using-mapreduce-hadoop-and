
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Class FineLock
 * Multi-threaded version of the sequential program with a lock on the objects within the 
 * data structure (and not the data structure itself) that keeps record of station-IDs 
 * and average TMAX temperatures
 */
public class FineLock {

	// HashMap to accumulate station IDs as keys and their corresponding counts and running average TMAX 
	// temperature as values
	private Map<String, double[]> accumulationDS = new HashMap<String, double[]>();
	private List<String> dataRecords = new ArrayList<String>();
	private Boolean setFibonacci = false;

	/*
	 * Constructor that takes the data records in the form of a list of strings that
	 * it need to process and a boolean value indicating whether to run fibonacci or not
	 */
	public FineLock(List<String> dataRecords, Boolean setFibonacci) {
		this.dataRecords = dataRecords;
		this.setFibonacci = setFibonacci;
	}
	

	/*
	 * Method to begin computing the average TMAX values per station and recording the necessary stats
	 */
	public void runFineLock() {
		
		// List to store time observed per execution 
		List<Double> timeRecords = new ArrayList<>();
		
		for(int i=0; i<10; i++) {
		
			// Check number of cores on the system:
			int processors = Runtime.getRuntime().availableProcessors();
			//System.out.println("Available CPU Cores: " + processors);
			
			// Total number of threads to be spawn = processors - 1
			int num_threads = ((processors > 2) ? (processors - 1) : 2);
			//System.out.println("Threads: " + num_threads);
			
			// Noting start time ...
			double start = System.currentTimeMillis();
						
			// Total Threads based on number of cores available on the machine
			Thread[] allThreads = new Thread[num_threads];
			int len = this.dataRecords.size();
			for(int j=0; j<num_threads; j++) {
				// Assigning a subset of the data records for each thread
				List<String> subRecords = this.dataRecords.subList((len*j)/num_threads, (len * (j+1))/ num_threads);
				// Spawning a thread to perform computation using only subset of the data records
				// The averages are updated in the same HashMap (accumulationDS)
				allThreads[j] = new Thread(new Runnable() {
					@Override
					public void run() {
						computeAverageTMAX(subRecords);
					}				
				});
				allThreads[j].start();
			}
			
			for (int j=0; j<num_threads; j++) {
				try {
					allThreads[j].join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			// Noting end time ...
			double end = System.currentTimeMillis();
			double total = end - start;
			// Adding the time observed for this run in the list timeRecords
			timeRecords.add(total);
		}
		
		// Display the average, min and max execution times
		Auxiliary.displayObservedTimes(timeRecords);
		//Auxiliary.displayTemps(this.accumulationDS);
	 }
	
	
	/*
	 * Method to perform actual computation and storing the results in a HashMap
	 * Also includes a Fibonacci function for part C of the problem
	 */
	public void computeAverageTMAX(List<String> subRecords) {
		for(String data : subRecords) {
					
			// Check if the record has TMAX value -
			if(data.toLowerCase().contains("TMAX".toLowerCase())) {
				String stnID = new String(data.split(",")[0]);
				double newTemp = Double.parseDouble(data.split(",")[3]);
				
				// If the record isn't present in the HashMap, create a new key-value pair
				if(!this.accumulationDS.containsKey(stnID)) {
					double[] temp_count = new double[2];
					temp_count[0] = newTemp;
					temp_count[1] = 1;
					this.accumulationDS.put(stnID, temp_count);
				}
				else {
					// Else take into account the TMAX temperature from the new record and compute new average
					// Adding a lock on the values in HashMap (accumulationDS)
					synchronized(this.accumulationDS.values()) { 
						
						double[] currentTempNCount = this.accumulationDS.get(stnID);
						double currentAvgTemp = currentTempNCount[0];
						double currentCount = currentTempNCount[1];
						
						// Computing New Average:
						double currentSum = currentAvgTemp * currentCount;
						double newSum = currentSum + newTemp;
						currentCount++;
						double newAvgTemp = newSum / (double)currentCount;
						currentTempNCount[0] = newAvgTemp;
						currentTempNCount[1] = currentCount;
						
						// Part C: Running Fibonacci(17) if necessary to slow down the updates on the temperature values
						if(this.setFibonacci) {
							//System.out.println("Running fibonacci");
							Auxiliary.executeFibonacci(17);
						}
						// Put the new avg back in the DS
						this.accumulationDS.put(stnID, currentTempNCount);
					}
				}
			}
		}
	}

	
	
}

