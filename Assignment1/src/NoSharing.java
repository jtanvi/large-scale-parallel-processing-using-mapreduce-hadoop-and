import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Class NoSharing
 * Per-thread data structure multi-threaded version of the sequential program
 * that assigns subsets of the input data records for processing by separate thread.
 * Each thread updates its own separate instance of the accumulation data structure.
 */
public class NoSharing {
	
	// HashMap to accumulate station IDs as keys and their corresponding counts and running average TMAX 
	// temperature as values. This hashmap results from combining all the other hashmaps (individual accumulation data structures)
	// processed by individual threads
	private Map<String, double[]> accumulationDS = new HashMap<String, double[]>();
	private List<String> dataRecords = new ArrayList<String>();
	private Boolean setFibonacci = false;
	
	/*
	 * Constructor that takes the data records in the form of a list of strings that
	 * it need to process and a boolean value indicating whether to run fibonacci or not
	 */
	public NoSharing(List<String> dataRecords, Boolean setFibonacci) {
		this.dataRecords = dataRecords;
		this.setFibonacci = setFibonacci;
	}
	
	/*
	 * Method to begin computing the average TMAX values per station and recording the necessary stats
	 */
	public void runNoSharing() {
		
		// List to store time observed per execution 
		List<Double> timeRecords = new ArrayList<>();
		
		for(int i=0; i<10; i++) {
			
			// Check number of cores on the system:
			int processors = Runtime.getRuntime().availableProcessors();
			//System.out.println("Available CPU Cores: " + processors);
			
			// Total number of threads to be spawn = processors - 1
			int num_threads = ((processors > 2) ? (processors - 1) : 2);
			//System.out.println("Threads: " + num_threads);
			
			// Noting start time ...
			double start = System.currentTimeMillis();
						
			// Total Threads based on number of cores available on the machine
			Thread[] allThreads = new Thread[num_threads];
			// Total instances of runnables to run each thread individually 
			RunNoSharing[] allRunners = new RunNoSharing[num_threads];
			int len = this.dataRecords.size();
			
			for(int j=0; j<num_threads; j++) {
				// Assigning a subset of the data records for each thread
				List<String> subRecords = this.dataRecords.subList((len*j)/num_threads, (len * (j+1))/ num_threads);
				// Creating an instance of a class (RunNoSharing) that implements Runnable and 
				// then having a thread work on that instance -
				allRunners[j] = new RunNoSharing(subRecords, this.setFibonacci);
				allThreads[j] = new Thread(allRunners[j]);
				allThreads[j].start();
			}
			
			for (int j=0; j<num_threads; j++) {
				try {
					allThreads[j].join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
			// Combining the individual accumulation data structures resulting from all threads processes
			// into a single data structure (accumulationDS)-
			for(int j=0; j<num_threads; j++) {
				// Get the accumulation data structure resulting from a process
				Map<String, double[]> partialAccRecords = allRunners[j].getDS(); 
				for (Map.Entry<String, double[]> entry : partialAccRecords.entrySet()) {
					String stnID = entry.getKey();
					double[] tempNCount = entry.getValue();
					
					// If the record isn't present in the HashMap, create a new key-value pair
					if(!this.accumulationDS.containsKey(stnID)) {
						this.accumulationDS.put(stnID, tempNCount);
					}
					// Else take into account the TMAX temperature from the new key-value pair and compute new average
					else {
						double[] currentTempNCount = this.accumulationDS.get(stnID);
						double currentTemp = currentTempNCount[0];
						double currentCount = currentTempNCount[1];
						double sum = (currentTemp * currentCount) + tempNCount[0];
						currentCount += tempNCount[1];
						currentTemp = sum / (double)currentCount;
						currentTempNCount[0] = currentTemp;
						currentTempNCount[1] = currentCount;
						this.accumulationDS.put(stnID, currentTempNCount);
					}
				}
			}
			
			// Noting end time ...
			double end = System.currentTimeMillis();
			double total = end - start;
			// Adding the time observed for this run in the list timeRecords
			timeRecords.add(total);
		}
		
		// Display the average, min and max execution times
		Auxiliary.displayObservedTimes(timeRecords);
		//Auxiliary.displayTemps(this.accumulationDS);
	}

	
}

class RunNoSharing implements Runnable {

	// HashMap to accumulate station IDs as keys and their corresponding counts and running average TMAX 
    // temperature as values.
	private Map<String, double[]> accumulationDS = new HashMap<String, double[]>();
	private List<String> records = new ArrayList<String>();
	private Boolean setFibonacci = false;
	
	/*
	 * Constructor that takes the data records in the form of a list of strings that
	 * it need to process and a boolean value indicating whether to run fibonacci or not
	 */
	public RunNoSharing(List<String> records, Boolean setFibonacci) {
		this.records = records;
		this.setFibonacci = setFibonacci;
	}
	
	// Method to return the data structure that stores the processed station ids and their 
	// corresponding counts and average TMAX temperature
	public Map<String, double[]> getDS() {
		return this.accumulationDS;
	}
	
	
	/*
	 * Method to perform actual computation and storing the results in a HashMap
	 * Also includes a Fibonacci function for part C of the problem
	 */
	public void computeAverageTMAX() {
		
		//Map<String, double[]> partialAccRecords = new HashMap<String, double[]>();
		for(String data : this.records) {
					
			// Check if the record has TMAX value -
			if(data.toLowerCase().contains("TMAX".toLowerCase())) {
				String stnID = new String(data.split(",")[0]);
				double newTemp = Double.parseDouble(data.split(",")[3]);
				
				// If the record isn't present in the HashMap, create a new key-value pair
				if(!this.accumulationDS.containsKey(stnID)) {
					double[] temp_count = new double[2];
					temp_count[0] = newTemp;
					temp_count[1] = 1;
					this.accumulationDS.put(stnID, temp_count);
				}
				// Else take into account the TMAX temperature from the new record and compute new average
				else {
					double[] currentTempNCount = this.accumulationDS.get(stnID);
					double currentAvgTemp = currentTempNCount[0];
					double currentCount = currentTempNCount[1];
					
					// Computing New Average:
					double currentSum = currentAvgTemp * currentCount;
					double newSum = currentSum + newTemp;
					currentCount++;
					double newAvgTemp = newSum / currentCount;
					currentTempNCount[0] = newAvgTemp;
					currentTempNCount[1] = currentCount;
					
					// Part C: Running Fibonacci(17) if necessary to slow down the updates on the temperature values
					if(this.setFibonacci) {
						//System.out.println("Running fibonacci");
						Auxiliary.executeFibonacci(17);
					}
					
					// Put the new avg back in the DS
					this.accumulationDS.put(stnID, currentTempNCount);

				}
			}
		}
		
	}

	
	@Override
	public void run() {
		this.computeAverageTMAX();
	}
	
}


