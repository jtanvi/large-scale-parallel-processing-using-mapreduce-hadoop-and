import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/*
 * Class Sequential
 * Calculating average TMAX temperatures by station ID in a sequential fashion without
 * using multi-threading
 */
public class Seq {
	
	// HashMap to accumulate station IDs as keys and their corresponding counts and running average TMAX 
	// temperature as values
	private Map<String, double[]> accumulationDS = new HashMap<String, double[]>();
	private Boolean setFibonacci = false;
	private List<String> dataRecords = new ArrayList<String>();

	/*
	 * Constructor that takes the data records in the form of a list of strings that
	 * it need to process and a boolean value indicating whether to run fibonacci or not
	 */
	public Seq(List<String> dataRecords, Boolean setFibonacci) {
		this.dataRecords = dataRecords;
		this.setFibonacci = setFibonacci;
	}
	
	/*
	 * Method to begin computing the average TMAX values per station and recording the necessary stats
	 */
	public void runSeq() {
		
		// List to store time observed per execution 
		List<Double> timeRecords = new ArrayList<>();
		
		for(int i=0; i<10; i++) {
			// Noting start time ...
			double start = System.currentTimeMillis();
			computeAverageTMAX();
			
			// Noting end time ...
			double end = System.currentTimeMillis();
			double total = end - start;
			timeRecords.add(total);
		}
		
		// Display the average, min and max execution times
		//Auxiliary.displayTemps(this.accumulationDS);
		Auxiliary.displayObservedTimes(timeRecords);
		
	}
	
	/*
	 * Method to perform actual computation and storing the results in a HashMap
	 * Also includes a Fibonacci function for part C of the problem
	 */
	public void computeAverageTMAX() {
		
		for(String data : this.dataRecords) {
			
			// Check if the record has TMAX value -
			if(data.toLowerCase().contains("TMAX".toLowerCase())) {
				String stnID = new String(data.split(",")[0]);
				double newTemp = Double.parseDouble(data.split(",")[3]);
				
				// If the record isn't present in the HashMap, create a new key-value pair
				if(!this.accumulationDS.containsKey(stnID)) {
					double[] temp_count = new double[2];
					temp_count[0] = newTemp;
					temp_count[1] = 1;
					this.accumulationDS.put(stnID, temp_count);
				}
				// Else take into account the TMAX temperature from the record and compute new average
				else {
					double[] currentTempNCount = this.accumulationDS.get(stnID);
					double currentAvgTemp = currentTempNCount[0];
					double currentCount = currentTempNCount[1];
					
					// Computing New Average:
					double currentSum = currentAvgTemp * currentCount;
					double newSum = currentSum + newTemp;
					currentCount++;
					double newAvgTemp = newSum / currentCount;
					currentTempNCount[0] = newAvgTemp;
					currentTempNCount[1] = currentCount;
					
					// Put the new average back in the HashMap
					this.accumulationDS.put(stnID, currentTempNCount);
					
					// Part C: Running Fibonacci(17) if necessary to slow down the updates on the temperature values
					if(this.setFibonacci) {
						Auxiliary.executeFibonacci(17);
					}
				}		
			}
		}
	}
}
