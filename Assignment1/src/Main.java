import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 * Program for running 5 versions of computing average of TMAX temperatures from given weather data
 * with and without using synchronization or locks
 */
public class Main {
	
	/*
	 *  Method to load the data into a list of strings where each string is a record consisting of
	 *  station ID, TMAX/TMIN keyword, temperature values etc.
	 */
	public static List<String> loadData(String fileName) {
			
			List<String> dataRecords = new ArrayList<String>();
			BufferedReader br = null;
			System.out.println("Loading data ........");
			
			try {
				String line = "";
	            br = new BufferedReader(new FileReader(fileName));
	            while ((line = br.readLine()) != null) {
	                dataRecords.add(line);
	            }
	
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (br != null) {
	                try {
	                    br.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
			
			return dataRecords;		
	}
	
	/*
	 * Runs the 5 versions for average TMAX computation, given the records and a boolean value
	 * indicating whether to compute fibonacci(17) while updating the computed records
	 */
	public static void runPrograms(List<String> dataRecords, Boolean setFibonacci) {
		
		System.out.println("Starting sequential run...");
		Seq seq = new Seq(dataRecords, setFibonacci);
		seq.runSeq();
		System.out.println();
		
		System.out.println("Starting No-Lock run...");
		NoLock noLock = new NoLock(dataRecords, setFibonacci);
		noLock.runNoLock();
		System.out.println();
		
		System.out.println("Starting Coarse-Lock run...");
		CoarseLock coarseLock = new CoarseLock(dataRecords, setFibonacci);
		coarseLock.runCoarseLock();
		System.out.println();
		
		System.out.println("Starting Fine-Lock run...");
		FineLock fineLock = new FineLock(dataRecords, setFibonacci);
		fineLock.runFineLock();
		System.out.println();
		
		System.out.println("Starting No-Sharing run...");
		NoSharing noSharing = new NoSharing(dataRecords, setFibonacci);
		noSharing.runNoSharing();		
	}
	
	public static void main(String[] args) throws IOException {

		// Load the weather data
	    if (args.length < 1) {
	      System.err.println("Please enter path for your data as an argument");
	      System.exit(2);
	    }
	    String filePath = args[0].toString();
		List<String> dataRecords = loadData(filePath); 
		
		// Computing average TMAX per station ID without running Fibonacci
		System.out.println("Speed observed without running Fibonacci...");
		runPrograms(dataRecords, false);
		System.out.println();
		System.out.println();
		// Computing average TMAX per station ID with running Fibonacci
		System.out.println("Speed observed with running Fibonacci...");
		runPrograms(dataRecords, true);
		
		System.out.println();
	}

}
